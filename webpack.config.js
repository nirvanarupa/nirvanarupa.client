const webpack = require("webpack");
const port = process.env.port || 3000;
const htmlwebpackplugin = require("html-webpack-plugin");
const path = require("path");
module.exports = {
  entry: ["./src/index.js"],
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
        exclude: /\.module\.css$/,
      },
      {
        test: /\.css$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
              modules: true,
            },
          },
        ],
        include: /\.module\.css$/,
      },
    ],
  },
  devServer: {
    open: true,
    port: port,
    historyApiFallback: true,
  },
  plugins: [new htmlwebpackplugin({ template: "./public/index.html" })],
};
