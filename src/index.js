import React from "react";
import { createRoot } from "react-dom/client";
const MainPage = document.querySelector(".root");
const root = createRoot(MainPage);
import Home from "./pages/Home";
import "./stylesheets/global.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
root.render(
  <>
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    </Router>
  </>
);
